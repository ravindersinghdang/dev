<?php echo $header;?>
<div class="main-container col-lg-12 col-xs-12">
<div class="container">
<h3>Manage Images</h3>
   <div class="add-button">
      <a href="#addModal" data-toggle="modal" class="btn btn-info">Add New</a>
   </div>
   <!-- Modal -->
   <div class="modal fade" id="addModal" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <form action="<?php echo URL::site('index/upload') ?>" method="POST" id="addImageForm">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal1-title">Add New Image</h4>
               </div>
               <div class="modal-body">
                  <div class="form-group">
                     <div class="col-lg-6">
                        <label>Title</label>
                        <input type="text" name="title" id="title" class="form-control" required>
                     </div>
                     <div class="col-lg-6">
                        <label>Image</label>
                        <input type="file" name="image" id="image" class="form-control" required>
                     </div>
                  </div>
               </div>
               <div class="modal-footer">
                  <button type="submit" id="add_image_btn" class="btn btn-success">Save Changes</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="modal fade" id="editModal" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <form action="<?php echo URL::site('index/editupload') ?>" method="POST" id="editImageForm">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal1-title">Edit Image</h4>
               </div>
               <div class="modal-body">
                  <div class="form-group">
                     <div class="col-lg-6">
                        <label>Title</label>
                        <input type="text" name="edit_title" id="edit_title" class="form-control" required>
                     </div>
                     <div class="col-lg-6">
                        <label>Image</label>
                        <input type="file" name="edit_image" id="edit_image" class="form-control">
                     </div>
                  </div>
               </div>
               <div class="modal-footer">
                  <input type="hidden" name="image_id" id="image_id" value="">
                  <button type="submit" id="edit_image_btn" class="btn btn-success">Save Changes</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="container table">
      <div class="table-responsive">
         <table class="table table-bordered table-inverse">
            <thead>
               <tr>
                  <th>Title</th>
                  <th>Thumbnail</th>
                  <th>Filename</th>
                  <th>Date added</th>
                  <th>Action</th>
               </tr>
            </thead>
            <tbody id="photoListTbl">
               <tr><td colspan="5">Loading...</td></tr>
            </tbody>
         </table>
      </div>
   </div>
   </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
   loadPhotoList();
});
</script>
<?php echo $footer;?>