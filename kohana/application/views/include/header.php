<!DOCTYPE html>
<html>
   <head>
      <title><?php echo $title; ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" type="text/css" href="<?php echo URL::base(); ?>assets/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo URL::base(); ?>assets/css/style.css">
      <link rel="stylesheet" href="<?php echo URL::base(); ?>assets/css/font-awesome.min.css">
      <script src="<?php echo URL::base(); ?>assets/js/jquery.min.js"></script>
      <script src="<?php echo URL::base(); ?>assets/js/bootstrap.min.js"></script>
      <script type="text/javascript">var siteUrl = '<?php echo URL::base(); ?>index.php/'; </script>
      <script src="<?php echo URL::base(); ?>assets/js/function.js"></script>
   </head>
   <body>