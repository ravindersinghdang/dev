<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Index extends Controller {

	public function action_index() {
		$template = View::factory('index/index');
		$template->header = View::factory('include/header', array("title" => "Home"));		
		$template->footer = View::factory('include/footer');
		$this->response->body($template);
	}

	public function action_upload() {
		$result = NULL;
        $filename = NULL;
        $response = NULL;
 
        if ($this->request->method() == Request::POST) {
            if (isset($_FILES['image'])) {
                $filename = $this->_save_image($_FILES['image']);
                if($filename) {
                	$params = array();
                	$params['title'] = $_POST['title'];
                	$params['filename'] = $filename;
                    $params['date_created'] =  Date::formatted_time();
                	$response = Model::factory('Photo')->addPhoto($params);
                    if($response) {
                        $result = "true";
                        $message = 'Image has been uploaded successfully.';
                    } else {
                        $result = "false";
                        $message = 'Error! Please try again.';    
                    }
                } else {
                    $result = "false";
                    $message = 'Error in uploading image. Make sure it is JPG/PNG/GIF file.';
                }
            } else {
                $result = "false";
                $message = 'Image is required! Please try again.';
            }
        } else {
            $result = "false";
            $message = 'Error! Invalid method used to post data.';
        }

        echo json_encode(array("result" => $result, "message" => $message));
        exit();
    }

	protected function _save_image($image) {
        if (! Upload::valid($image) || ! Upload::not_empty($image) || ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif'))) {
            return false;
        }
        $directory = DOCROOT.'uploads';
        $path_parts = pathinfo($image['name']);
        $filename = strtolower(time().'_'.Text::random('alnum', 20)).'.'.$path_parts['extension'];

        if($file = Upload::save($image, $filename, $directory.'/large')) {
            Image::factory($file)->resize(100, 100, Image::AUTO)->save($directory.'/thumb/'.$filename);
            return $filename;
        }
 
        return false;
    }

    public function action_list() {
    	$rows = Model::factory('Photo')->listPhoto();
    	if($rows && count($rows) > 0) {
    		$data = array();

    		$i = 0;
    		foreach($rows as $row) {
    			$data[$i]['title'] = stripslashes($row['title']);
    			$data[$i]['thumbnail'] = URL::base().'uploads/thumb/'.stripslashes($row['filename']);
    			$data[$i]['filename'] = stripslashes($row['filename']);
    			$data[$i]['date_created'] = date("m/d/Y h:i a", strtotime($row['date_created']));
    			$data[$i]['id'] = stripslashes($row['image_pid']);
    			$i++;
    		}

    		echo json_encode(array("result" => "true", "data" => $data));
    		exit();
    	} else {
    		echo json_encode(array("result" => "false", "message" => "No records found."));
    		exit();
    	}    	
    }

    public function action_delete() {
        $result = NULL;
        $response = NULL;
 
        if ($this->request->method() == Request::POST) {
            if (isset($_POST['image_id'])) {
                $row = Model::factory('Photo')->getPhoto($_POST['image_id']);
                $row = $row->as_array();
                if($row) {
                    $directory = DOCROOT.'uploads';

                    if(file_exists($directory.'/large/'.$row[0]['filename'])) {
                        unlink($directory.'/large/'.$row[0]['filename']);
                    }

                    if(file_exists($directory.'/thumb/'.$row[0]['filename'])) {
                        unlink($directory.'/thumb/'.$row[0]['filename']);
                    }

                    $response = Model::factory('Photo')->deletePhoto($_POST['image_id']);

                    if($response) {
                        $result = "true";
                        $message = 'Image has been deleted successfully.';
                    } else {
                        $result = "false";
                        $message = 'Error! Please try again.';
                    }
                } else {
                    $result = "false";
                    $message = 'Error! Record not found.';
                }
            } else {
                $result = "false";
                $message = 'Error! Invalid data.';
            }
        } else {
            $result = "false";
            $message = 'Error! Invalid method used to post data.';
        }
 
        echo json_encode(array("result" => $result, "message" => $message));
        exit();
    }

    public function action_editupload() {
        $result = NULL;
        $filename = NULL;
        $response = NULL;
 
        if ($this->request->method() == Request::POST) {
            if($_POST['image_id']) {
                $row = Model::factory('Photo')->getPhoto($_POST['image_id']);
                $row = $row->as_array();
                if($row) {
                    if (isset($_FILES['image'])) {
                        $filename = $this->_save_image($_FILES['image']);
                        if(!$filename) {
                            $result = "false";
                            $message = 'Error in uploading image. Make sure it is JPG/PNG/GIF file.';
                        }
                    }

                    $params = array();
                    $params['title'] = $_POST['title'];

                    if($filename) {
                        $params['filename'] = $filename;
                    }

                    $response = Model::factory('Photo')->editPhoto($params, $_POST['image_id']);
                    if($response) {
                        $result = "true";
                        $message = 'Record has been updated successfully.';
                    } else {
                        $result = "false";
                        $message = 'Error! Please try again.';
                    }
                } else {
                    $result = "false";
                    $message = 'Error! Record not found.';
                }
            } else {
                $result = "false";
                $message = 'Error! Invalid data.';
            }
        } else {
            $result = "false";
            $message = 'Error! Invalid method used to post data.';
        }
 
        echo json_encode(array("result" => $result, "message" => $message));
        exit();
    }
}