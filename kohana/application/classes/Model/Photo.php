<?php
class Model_Photo extends Model {

    public function addPhoto($params) {
        try {
            return DB::insert('tbl_images', array_keys($params))->values(array_values($params))->execute();
        } catch(Exception $e) {
            return false;
        }
    }

    public function listPhoto() {
        try {
        	return DB::select()->from('tbl_images')->order_by('image_pid', 'DESC')->execute();
        } catch(Exception $e) {
            return false;
        }    
    }

    public function getPhoto($image_id) {
        try {
            return DB::select()->from('tbl_images')->where('image_pid', '=', $image_id)->execute();
        } catch(Exception $e) {
            return false;
        }
    }

    public function deletePhoto($image_id) {
        try {
            return DB::delete('tbl_images')->where('image_pid', '=', $image_id)->execute();
        } catch(Exception $e) {
            return false;
        }
    }

    public function editPhoto($params, $image_id) {
        try {
            return DB::update('tbl_images')->set($params)->where('image_pid', '=', $image_id)->execute();
        } catch(Exception $e) {
            return false;
        }
    }
}
?>