var error_message = "Error! Please try again.";

$(document).ready(function() {
	$(document).on("submit", "#addImageForm", function(e) {
		e.preventDefault();

		var image = $('#image').prop('files')[0];
        var title = $("#title").val();
        var url = $(this).attr("action");

        if(!image) {
            alert("Image is required!");
            return false;
        }

        if(!$.trim(title)) {
            alert("Title is required!");
            return false;   
        }

        var formData = new FormData();
		formData.append('image', image);
		formData.append('title', title);

		showLoading();

        $.ajax({
            url: url,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: formData,                         
            type: 'post',
            success: function(response) {
                hideLoading();

                if(response.result == "true") {
                    $("#title,#image").val('');
                	$("#addModal").modal("hide");
                    loadPhotoList();
                } else {
                    alert(response.message);
                }
            },
            error: function() {
                hideLoading();
                alert(error_message);
            }
        });
	});

    $(document).on("click", ".delete_photo", function() {
        var obj = $(this);
        var image_id = $(this).attr("data");

        if(!$.trim(image_id)) {
            alert("Invalid record! Please try again.");
            return false;   
        }

        if(confirm("Do you want to delete this image?")) {
            showLoading();
            $.ajax({
                url: siteUrl+'index/delete',
                type: 'POST',
                dataType: 'json',
                data: {image_id: image_id},
                success: function(response) {
                    hideLoading();
                    if(response.result == "true") {
                        obj.parents("tr").remove();
                        if($("#photoListTbl tr").size() <= 0) {
                            $("#photoListTbl").html('<tr><td colspan="5">No records found.</td></tr>');
                        }
                    } else {
                        alert(response.message);
                    }
                },
                error: function() {
                    hideLoading();
                    alert(error_message);
                }
            });
        }
    });

    $(document).on("click", ".edit_photo", function() {
        var image_id = $(this).attr("data");
        var title = $(this).parents("tr").find("td:first").html();

        if(!$.trim(image_id)) {
            alert("Invalid record! Please try again.");
            return false;   
        }

        $("#edit_image").val('');
        $("#edit_title").val(title);
        $("#image_id").val(image_id);

        $("#editModal").modal("show");
    });

    $(document).on("submit", "#editImageForm", function(e) {
        e.preventDefault();

        var image = $('#edit_image').prop('files')[0];
        var title = $("#edit_title").val();
        var image_id = $("#image_id").val();

        if(!$.trim(image_id)) {
            alert("Invalid record! Please try again.");
            return false;   
        }

        if(!$.trim(title)) {
            alert("Title is required!");
            return false;   
        }

        var formData = new FormData();
        formData.append('image', image);
        formData.append('title', title);
        formData.append('image_id', image_id);

        var url = $(this).attr("action");

        showLoading();

        $.ajax({
            url: url,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: formData,                         
            type: 'post',
            success: function(response) {
                hideLoading();

                if(response.result == "true") {
                    $("#edit_title,#edit_image").val('');
                    $("#editModal").modal("hide");
                    loadPhotoList();
                } else {
                    alert(response.message);
                }
            },
            error: function() {
                hideLoading();
                alert(error_message);
            }
        });
    });
});

function loadPhotoList() {
	$("#photoListTbl").html('<tr><td colspan="5">Loading...</td></tr>');

	$.ajax({
        url: siteUrl+'index/list',
        dataType: 'json',
        data: 1,
        success: function(response) {
            if(response.result == "true") {
            	var html = '';
            	$.each(response.data, function(i, v) {
            		html += '<tr>';
            		html += '<td>'+v.title+'</td>';
            		html += '<td><img src="'+v.thumbnail+'"></td>';
            		html += '<td>'+v.filename+'</td>';
            		html += '<td>'+v.date_created+'</td>';
            		html += '<td><a href="javascript:;" class="edit_photo" data="'+v.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a href="javascript:;" class="delete_photo" data="'+v.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>';
            		html += '</tr>';
            	});
            	$("#photoListTbl").html(html);
            } else {
                $("#photoListTbl").html('<tr><td colspan="5">'+response.message+'</td></tr>');
            }
        },
        error: function() {
            alert(error_message);
        }
    });
}

function showLoading() {
	$(".loading").show();
}

function hideLoading() {
	$(".loading").hide();
}